-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:49
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `method` set('0','1','2','3') NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL,
  `ip` bigint(20) NOT NULL DEFAULT '0',
  `ua` varchar(32) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_log`
--

INSERT DELAYED INTO `user_log` (`id`, `id_user`, `method`, `time`, `ip`, `ua`) VALUES
(1, 1, '1', 1418195418, 2130706433, 'Chrome 37.0.2062.124'),
(2, 1, '1', 1418195943, 2130706433, 'Chrome 37.0.2062.124'),
(3, 1, '2', 1418318139, 2130706433, 'Chrome 37.0.2062.124'),
(4, 1, '1', 1418319758, 2130706433, 'Chrome 37.0.2062.124'),
(5, 1, '1', 1418322531, 2130706433, 'Chrome 37.0.2062.124'),
(6, 1, '1', 1418322829, 2130706433, 'Chrome 37.0.2062.124'),
(7, 2, '2', 1418362594, 2130706433, 'Chrome 37.0.2062.124'),
(8, 1, '1', 1418363118, 2130706433, 'Chrome 37.0.2062.124'),
(9, 1, '2', 1418381662, 2130706433, 'Chrome 37.0.2062.124'),
(10, 1, '2', 1418446486, 2130706433, 'Chrome 37.0.2062.124'),
(11, 1, '1', 1418446539, 2130706433, 'Chrome 37.0.2062.124'),
(12, 1, '1', 1418448197, 2130706433, 'Chrome 37.0.2062.124'),
(13, 1, '2', 1418495936, 2130706433, 'Chrome 37.0.2062.124'),
(14, 4, '3', 1418634033, 2130706433, 'Chrome 37.0.2062.124'),
(15, 4, '3', 1418645048, 2130706433, 'Chrome 37.0.2062.124'),
(16, 4, '3', 1418726479, 2130706433, 'Chrome 37.0.2062.124'),
(17, 1, '1', 1418726504, 2130706433, 'Chrome 37.0.2062.124'),
(18, 1, '2', 1418770468, 2130706433, 'Chrome 37.0.2062.124'),
(19, 1, '2', 1418770859, 2130706433, 'Chrome 37.0.2062.124'),
(20, 1, '2', 1418777558, 2130706433, 'Chrome 37.0.2062.124'),
(21, 1, '2', 1418800095, 2130706433, 'Chrome 37.0.2062.124'),
(22, 1, '2', 1418964754, 2130706433, 'Chrome 37.0.2062.124');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`), ADD KEY `id_user` (`id_user`), ADD KEY `time` (`time`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
